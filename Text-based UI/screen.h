/*

Copyright © 2014 Mathias Pihl <mathias@pihlerne.dk>
This work is free. You can redistribute it and/or modify it under the
terms of the Do What The Fuck You Want To Public License, Version 2,
as published by Sam Hocevar. See the COPYING file for more details.

*/

/*
COLORS AVAILABLE CROSS-PLATFORM:
BLACK
BLUE
GREEN
CYAN
RED
YELLOW
WHITE
USE PLATFORM-DEPENDENT COLORS WITH CAUTION
*/

#ifndef __SCREEN__
#define __SCREEN__

#define WIN 0
#define UNIX 1

#if defined (_WIN32) || defined (WIN32)
	#define OS_WIN
	#ifndef _WIN32_WINNT
		#define _WIN32_WINNT 0x0500
	#endif /*_WIN32_WINNT*/
	#include <windows.h>
	#include <iostream>
	#define COLOR_BLACK 0
	#define COLOR_BLUE 1
	#define COLOR_GREEN 2
	#define COLOR_CYAN 3
	#define COLOR_RED 4
	#define COLOR_YELLOW 6
	#define COLOR_WHITE 7
#else
	#define OS_UNIX
	#include <ncurses.h>
#endif /*defined (_WIN32) || defined (WIN32)*/

#include <string>

class Screen
{
	private:
		static int colorCount;
		#if defined (OS_WIN)
		static int bColor;
		static int fColor;
		static int cursorY;
		static int cursorX;
		static HANDLE hOutput;
		static char screen[];
		static unsigned short screenColor[];
		#endif
	public:
		static void Init();
		static void Refresh();
		static void Clear();
		static void Write(std::string);
		static void Print(std::string);
		static void GetResolution(int&, int&);
		static void Move(int, int);
		static void SetCursor(bool);
		static void SetColor(int, int);
		static void End();
		
};

#endif /*__SCREEN__*/
