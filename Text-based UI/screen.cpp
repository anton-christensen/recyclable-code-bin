#include "screen.h"

int Screen::colorCount;
#if defined (OS_WIN)
int Screen::fColor;
int Screen::bColor;
int Screen::cursorY;
int Screen::cursorX;
HANDLE Screen::hOutput;
char Screen::screen[100000]; // 24000 characters needed for 1920x1200 monitor (240*100 characters)
unsigned short Screen::screenColor[100000];
#endif

void Screen::Init() // Inits systems and prepares framebuffer
{
	#if defined (OS_UNIX)
		colorCount = 1;
		initscr();
		start_color();
	#endif
	#if defined (OS_WIN)
		hOutput = GetStdHandle(STD_OUTPUT_HANDLE);
		Clear();
	#endif
	SetColor(COLOR_BLACK, COLOR_WHITE);
}

void Screen::Refresh() // Refreshes changes to screen. CHANGES WILL NOT APPEAR UNTIL CALLED
{
	#if defined (OS_UNIX)
		refresh();
	#endif
	#if defined (OS_WIN)
		COORD pos = {0, 0};
		SetConsoleCursorPosition(hOutput, pos);
		int x, y;
		GetResolution(x, y);
		for(int i = 0; i < y; i++) 
		{
			for(int j = 0; j < x; j++) 
			{
				SetConsoleTextAttribute(hOutput, screenColor[(i*x)+j]);
				std::cout << screen[(i*x)+j];
			}
			if(y-1 != i) std::cout << std::endl;
		}
	#endif
}

void Screen::Clear() // Fills the screen with black
{
	#if defined (OS_UNIX)
		clear();
	#endif
	#if defined (OS_WIN)
		int screenSizeX, screenSizeY;
		GetResolution(screenSizeX, screenSizeY);
		for(int i = 0; i < (screenSizeX*screenSizeY)-1; i++) {
			screen[(cursorY * screenSizeX) + cursorX] = ' ';
			screenColor[(cursorY * screenSizeX) + cursorX] = 0x07;
			cursorX++;
			if(cursorX >= screenSizeX) {
				cursorX = 0;
				cursorY++;
			}
		}
	#endif
}

void Screen::SetCursor(bool state) // Sets whether the cursor should be visible or not. False/0 for hidden
{
	#if defined (OS_UNIX)	
		curs_set(state);
	#endif
	#if defined (OS_WIN)
		CONSOLE_CURSOR_INFO info;
		info.bVisible = state;
		info.dwSize = 1;
		SetConsoleCursorInfo(hOutput,&info);
	#endif
}

void Screen::Write(std::string str) // Writes text to screen, without adding a newline
{
	#if defined (OS_UNIX)
		printw(str.c_str());
	#endif
	#if defined (OS_WIN)
		int screenSizeX, screenSizeY;
		GetResolution(screenSizeX, screenSizeY);
		for(int i = 0; i < str.size(); i++) {
			screen[(cursorY * screenSizeX) + cursorX] = str[i];
			screenColor[(cursorY * screenSizeX) + cursorX] = (unsigned) fColor<<4|(unsigned)bColor;
			cursorX++;
			if(cursorX >= screenSizeX) {
				cursorX = 0;
				cursorY++;
			}
		}
	#endif
}

void Screen::Print(std::string str) // Writes text to screen, and adds a newline
{
	Write(str.c_str());
	#if defined (OS_WIN)
		cursorX = 0;
		cursorY++;
	#endif
}

void Screen::SetColor(int backgroundColor, int foregroundColor) // Change the color of the text and/or background
{	
	#if defined (OS_UNIX)
		if(colorCount != 1) attroff(colorCount-1);
		init_pair(colorCount, backgroundColor, foregroundColor);
		attron(COLOR_PAIR(colorCount));
		colorCount++;
	#endif
	#if defined (OS_WIN)
		fColor = foregroundColor % 16;
		bColor = backgroundColor % 16;
	#endif
}

void Screen::GetResolution(int &x, int &y) // Returns the resolution of the window, IN CHARACTERS
{
	#if defined (OS_UNIX)
		getmaxyx(stdscr, x, y);
	#endif
	#if defined (OS_WIN)
		CONSOLE_SCREEN_BUFFER_INFO csbi;
		GetConsoleScreenBufferInfo(hOutput, &csbi);
		x = csbi.srWindow.Right - csbi.srWindow.Left + 1;
		y = csbi.srWindow.Bottom - csbi.srWindow.Top + 1;
	#endif
}

void Screen::Move(int x, int y) // Move the cursor to a specific point on the screen. x and y is in characters
{
	#if defined (OS_UNIX)
		move(x, y);
	#endif
	#if defined (OS_WIN)
		cursorX = x;
		cursorY = y;
	#endif
}

void Screen::End() // Run at end of program, to unload stuff
{
	#if defined (OS_UNIX)
		endwin();
	#endif
	#if defined (OS_WIN)
		int x, y;
		GetResolution(x,y);
		Move(0,y);
		SetColor(COLOR_WHITE, COLOR_BLACK);
		Write(" ");
		SetConsoleTextAttribute(hOutput, screenColor[y*x]);
	#endif
}
