#include "screen.h"

#if defined (_WIN32) || defined (WIN32)
#include <windows.h>
#define Sleep(x) Sleep(x)   /* Windows version */
#else
#include <unistd.h>
#define Sleep(x) usleep(x*1000)   /* Linux version */

#endif

int main()
{
	Screen::Init();
	int x,y;
	Screen::GetResolution(x,y);
	Screen::SetColor(COLOR_YELLOW, COLOR_RED);
	for(int i = 0; i < x; i++)
	{
		Screen::Move(i, 0);
		Screen::Print("#");
		
		Screen::Move(i, y-1);
		Screen::Print("#");
	}
	for(int i = 0; i < y; i++)
	{
		Screen::Move(0, i);
		Screen::Print("#");
		
		Screen::Move(x-1, i);
		Screen::Print("#");
	}
	Screen::Refresh();
	Sleep(2000);
	Screen::End();
	return 0;
}
