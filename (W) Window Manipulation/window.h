#ifndef __WINDOW
#define __WINDOW

#ifndef _WIN32_WINNT
#define _WIN32_WINNT 0x0500
#endif /*_WIN32_WINNT*/

#include <Windows.h>
#include <string>
#include <iostream>

using namespace std;

class Window
{
	private:
		HWND windowHandle;
		COORD size;
		  string screen;
		  string screenColor[2048];
		  int setColor;
		  int screenX, screenY, screenSizeX, screenSizeY;
	public:
		Window();
		  void OnRender();
		bool Move(int x, int y);
		bool Resize(short int x, short int y);
		void RemoveCursor();
		  COORD GetWindowPos();
		  bool GotoXY(int x, int y);
		void GetMainMonitorRes(int &X, int &Y);
		  void SetTextColor(int);
		  bool WriteMap(int,int,string);
		HWND GetHandle();
		  void InitScreen();
		  void Write(int,int,int);
		  void Write(int,int,string);
		  void Write(int,int,int,int);
		  void Write(int,int,int,string);
};

#endif